import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:youthapp/constant/custom_colors.dart';

class ImageGrid extends StatefulWidget {
  @override
  _ImageGridState createState() => _ImageGridState();
  List<Map<String, Object>> icon_data = [
    {
      'icon_url': 'assets/happyIcon.png',
      'icon_text': 'Amazing',
    },
    {
      'icon_url': 'assets/happyIcon.png',
      'icon_text': 'Happy',
    },
    {
      'icon_url': 'assets/goodIcon.png',
      'icon_text': 'Good',
    },
    {
      'icon_url': 'assets/024Happy1.png',
      'icon_text': 'Okay',
    },
    {
      'icon_url': 'assets/okayIcon.png',
      'icon_text': 'Not sure',
    },
    {
      'icon_url': 'assets/sadIcon.png',
      'icon_text': 'Sad',
    },
    {
      'icon_url': 'assets/terribleIcon.png',
      'icon_text': 'Terrible',
    },
    {
      'icon_url': 'assets/helpIcon.png',
      'icon_text': 'I need help',
    },
  ];
}

class _ImageGridState extends State<ImageGrid> {
  int _selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, childAspectRatio: 1.5, mainAxisSpacing: 12.h),
        itemCount: widget.icon_data.length,
        itemBuilder: (BuildContext context, int index) {
          // return GridTileBar(
          //   title: InkWell(
          //     onTap: () {
          //       setState(() {
          //         for (int i = 0; i < widget.icon_data.length; i++) {
          //           widget.icon_data[i]['active'] = false;
          //         }
          //         widget.icon_data[index]['active'] = true;
          //       });
          //     },
          //     child: widget.icon_data[index]['active']
          //         ? Stack(children: [
          //             Center(
          //               child: CircleAvatar(
          //                 radius: 50.w,
          //                 backgroundColor: Color.fromRGBO(232, 67, 128, 0.7),
          //                 child: CircleAvatar(
          //                   radius: 46.w,
          //                   backgroundColor: Colors.white,
          //                   child: CircleAvatar(
          //                     radius: 42.w,
          //                     child: Image.asset(
          //                       widget.icon_data[index]['icon_url'],
          //                       fit: BoxFit.contain,
          //                     ),
          //                   ),
          //                 ),
          //               ),
          //             ),
          //             Center(
          //               child: Platform.isAndroid
          //                   ? Padding(
          //                       padding: EdgeInsets.only(top: 10.h),
          //                       child: CircleAvatar(
          //                         backgroundColor:
          //                             Color.fromRGBO(232, 67, 128, 0.85),
          //                         radius: 42.w,
          //                       ),
          //                     )
          //                   : Padding(
          //                       padding: EdgeInsets.only(top: 7.h),
          //                       child: CircleAvatar(
          //                         backgroundColor:
          //                             Color.fromRGBO(232, 67, 128, 0.85),
          //                         radius: 42.w,
          //                       ),
          //                     ),
          //             ),
          //             Center(
          //               child: Padding(
          //                   padding: EdgeInsets.only(top: 35.w),
          //                   child: Image.asset('assets/path973.png',
          //                       height: 30.h, width: 35.w)),
          //             )
          //           ])
          //         : Center(
          //             child: CircleAvatar(
          //               radius: 42.w,
          //               child: Image.asset(
          //                 widget.icon_data[index]['icon_url'],
          //                 fit: BoxFit.contain,
          //               ),
          //             ),
          //           ),
          //   ),
          //   subtitle: Align(
          //     child: Text(
          //       widget.icon_data[index]['icon_text'],
          //       style: TextStyle(color: blackFour, fontSize: 15.sp),
          //       textAlign: TextAlign.center,
          //     ),
          //   ),
          //   // backgroundColor: Colors.red,
          // );
          return GridTileBar(
            title: InkWell(
              onTap: () {
                setState(() {
                  _selectedIndex = index;
                });
              },
              child: _selectedIndex == index
                  ? Stack(children: [
                      Center(
                        child: CircleAvatar(
                          radius: 50.w,
                          backgroundColor: Color.fromRGBO(232, 67, 128, 0.7),
                          child: CircleAvatar(
                            radius: 46.w,
                            backgroundColor: Colors.white,
                            child: CircleAvatar(
                              radius: 42.w,
                              child: Image.asset(
                                widget.icon_data[index]['icon_url'],
                                fit: BoxFit.contain,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Center(
                        child: Platform.isAndroid
                            ? Padding(
                                padding: EdgeInsets.only(top: 8.1.h),
                                child: CircleAvatar(
                                  backgroundColor:
                                      Color.fromRGBO(232, 67, 128, 0.85),
                                  radius: 42.w,
                                ),
                              )
                            : Padding(
                                padding: EdgeInsets.only(top: 7.h),
                                child: CircleAvatar(
                                  backgroundColor:
                                      Color.fromRGBO(232, 67, 128, 0.85),
                                  radius: 42.w,
                                ),
                              ),
                      ),
                      Center(
                        child: Padding(
                            padding: EdgeInsets.only(top: 35.w),
                            child: Image.asset('assets/path973.png',
                                height: 30.h, width: 35.w)),
                      )
                    ])
                  : Center(
                      child: CircleAvatar(
                        radius: 42.w,
                        child: Image.asset(
                          widget.icon_data[index]['icon_url'],
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
            ),
            subtitle: Align(
              child: Text(
                widget.icon_data[index]['icon_text'],
                style: TextStyle(color: blackFour, fontSize: 15.sp),
                textAlign: TextAlign.center,
              ),
            ),
            // backgroundColor: Colors.red,
          );
        });
  }
}
