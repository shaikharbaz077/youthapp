import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:youthapp/constant/custom_colors.dart';

class Address extends StatelessWidget {
  const Address({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(top: 15.h),
          child: Text(
            'Address',
            style: TextStyle(
                fontSize: 18.sp, color: blackFour, fontWeight: FontWeight.w600),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 8.h),
          child: Text(
              'The address of this agency will be displayed here. This is a dummy text and needs to be replaced with actual location.'),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 12.h,
          ),
          child: Image.asset(
            'assets/maskGroup5.png',
            fit: BoxFit.fill,
          ),
        ),
      ],
    );
  }
}
