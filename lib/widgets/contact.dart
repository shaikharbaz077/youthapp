import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:youthapp/constant/custom_colors.dart';

class Contact extends StatelessWidget {
  const Contact({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(top: 15.h),
          child: Text(
            'Contact details',
            style: TextStyle(
                fontSize: 18.sp, color: blackFour, fontWeight: FontWeight.w600),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8.h,
          ),
          child: Row(
            children: [
              Text(
                'Email: ',
                style: TextStyle(fontSize: 15.sp, color: greyishBrownThree),
              ),
              Text(
                'coordinator@balallyfrc.ie',
                style: TextStyle(
                  fontSize: 15.sp,
                  color: coolBlueTwo,
                  decoration: TextDecoration.underline,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8.h,
          ),
          child: Row(
            children: [
              Text(
                'Phone: ',
                style: TextStyle(fontSize: 15.sp, color: greyishBrownThree),
              ),
              Text(
                '086 785 8787',
                style: TextStyle(
                  fontSize: 15.sp,
                  color: coolBlueTwo,
                  decoration: TextDecoration.underline,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 8.h,
          ),
          child: Row(
            children: [
              Text(
                'Website: ',
                style: TextStyle(fontSize: 15.sp, color: greyishBrownThree),
              ),
              Text(
                'www.balallyfrc.ie',
                style: TextStyle(
                  fontSize: 15.sp,
                  color: coolBlueTwo,
                  decoration: TextDecoration.underline,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
