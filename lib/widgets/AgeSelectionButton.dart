import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:youthapp/constant/custom_colors.dart';

class AgeSelectionButton extends StatefulWidget {
  @override
  _AgeSelectionButtonState createState() => _AgeSelectionButtonState();
  final active_color = Color.fromRGBO(232, 67, 128, 1.0);
  final inactive_color = whiteTwelve;
}

class _AgeSelectionButtonState extends State<AgeSelectionButton> {
  bool _button_1_state = true;
  bool _button_2_state = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 300.w,
          height: 50.h,
          child: RaisedButton(
            color:
                _button_1_state ? widget.active_color : widget.inactive_color,
            onPressed: () {
              setState(() {
                _button_1_state = true;
                _button_2_state = false;
              });
            },
            child: Text(
              '10 - 16 yrs',
              style: TextStyle(
                  color: _button_1_state ? Colors.white : blackFour,
                  fontSize: 16.sp),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 15.h),
          width: 300.w,
          height: 50.h,
          child: RaisedButton(
            color:
                _button_2_state ? widget.active_color : widget.inactive_color,
            onPressed: () {
              setState(() {
                _button_1_state = false;
                _button_2_state = true;
              });
            },
            child: Text(
              '17 – 24 yrs',
              style: TextStyle(
                  color: _button_2_state ? Colors.white : blackFour,
                  fontSize: 16.sp),
            ),
          ),
        ),
      ],
    );
  }
}
