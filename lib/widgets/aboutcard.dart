import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';
import 'package:youthapp/constant/custom_colors.dart';

class AboutCard extends StatelessWidget {
  final support = [
    'Okay',
    'Sad',
    'Terrible',
  ];
  final title;

  AboutCard({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(top: 15.h),
          child: Text(
            title,
            style: TextStyle(
                fontSize: 16.sp, color: blackFour, fontWeight: FontWeight.w600),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 12.h),
          child: Wrap(
            spacing: 10.w,
            runSpacing: 10.h,
            children: [
              ...support
                  .map(
                    (e) => Container(
                      padding: EdgeInsets.symmetric(
                        vertical: 10.h,
                        horizontal: 10.w,
                      ),
                      color: whiteTwelve,
                      child: Text(
                        e,
                        style: TextStyle(fontSize: 15.sp, color: blackFour),
                      ),
                    ),
                  )
                  .toList()
            ],
          ),
        ),
      ],
    );
  }
}
