import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:youthapp/constant/custom_colors.dart';

class CustomFilterChip extends StatefulWidget {
  var text;
  CustomFilterChip(this.text);
  @override
  _CustomFilterChipState createState() => _CustomFilterChipState();
}

class _CustomFilterChipState extends State<CustomFilterChip> {
  @override
  Widget build(BuildContext context) {
    return ChoiceChip(
      label: Text(widget.text['label_name']),
      labelPadding: EdgeInsets.symmetric(vertical: 7.h, horizontal: 10.w),
      selected: widget.text['active'],
      selectedShadowColor: whiteTwelve,
      backgroundColor: whiteTwelve,
      onSelected: (bool selected) {
        if (selected) {
          setState(() {
            widget.text['active'] = true;
          });
        } else {
          setState(() {
            widget.text['active'] = false;
          });
        }
      },
      selectedColor: Color.fromRGBO(232, 67, 128, 1.0),
      labelStyle: !widget.text['active']
          ? TextStyle(
              color: blackFour,
              fontSize: 13.sp,
            )
          : TextStyle(
              color: Colors.white,
              fontSize: 13.sp,
            ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.r)),
    );
  }
}
