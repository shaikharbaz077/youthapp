import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class ImageLoad extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 15.w, top: 40.h),
      child: InkWell(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: Image.asset(
          'assets/path738_backword_white.png',
          height: 17.h,
          width: 9.w,
        ),
      ),
    );
  }
}
