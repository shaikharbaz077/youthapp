import 'package:flutter/material.dart';

// ignore: must_be_immutable
class SimpleText extends StatelessWidget {
  final String title;
  final int size;
  final color;
  bool softWrap;
  TextOverflow textOverflow;
  int maxLines;
  TextDirection textDirection;
  double textScaleFactor;
  TextStyle textStyle;
  Locale locale;
  String semanticsLabel;

  SimpleText(
      {this.title,
      this.color,
      this.size,
      this.textStyle,
      this.maxLines,
      this.locale,
      this.semanticsLabel,
      this.softWrap,
      this.textDirection,
      this.textOverflow,
      this.textScaleFactor});

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: textStyle,
      maxLines: maxLines,
      semanticsLabel: semanticsLabel,
      overflow: textOverflow,
      locale: locale,
      softWrap: softWrap,
      textDirection: textDirection,
      textScaleFactor: textScaleFactor,
    );
  }
}
