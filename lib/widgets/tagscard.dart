import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:youthapp/constant/custom_colors.dart';

class Tags extends StatelessWidget {
  final tags = [
    'Financial Worries',
    'Anger',
    'Grief',
    'Social Media Support',
    'Financial Worries',
  ];

  Tags({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(top: 15.h),
          child: Text(
            'Tags',
            style: TextStyle(
                fontSize: 16.sp, color: blackFour, fontWeight: FontWeight.w600),
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 12.h),
          child: Wrap(
            spacing: 10.w,
            runSpacing: 10.h,
            children: [
              ...tags
                  .map(
                    (e) => Container(
                      padding: EdgeInsets.symmetric(
                        vertical: 10.h,
                        horizontal: 10.w,
                      ),
                      color: whiteTwelve,
                      child: Text(
                        e,
                        style: TextStyle(fontSize: 15.sp, color: blackFour),
                      ),
                    ),
                  )
                  .toList()
            ],
          ),
        ),
      ],
    );
  }
}
