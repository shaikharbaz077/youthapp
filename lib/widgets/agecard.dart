import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:youthapp/constant/custom_colors.dart';

class AgeCard extends StatelessWidget {
  const AgeCard({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.only(top: 15.h),
          child: Text(
            'Age range',
            style: TextStyle(
                fontSize: 16.sp, color: blackFour, fontWeight: FontWeight.w600),
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            top: 12.h,
          ),
          padding: EdgeInsets.symmetric(
            vertical: 10.h,
            horizontal: 10.w,
          ),
          color: whiteTwelve,
          child: Text(
            '10 - 17yrs',
            style: TextStyle(fontSize: 15.sp, color: blackFour),
          ),
        ),
      ],
    );
  }
}
