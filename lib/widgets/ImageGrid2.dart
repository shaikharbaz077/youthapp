import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:youthapp/constant/custom_colors.dart';

class ImageGrid2 extends StatefulWidget {
  @override
  _ImageGrid2State createState() => _ImageGrid2State();
  List<Map<String, Object>> icon_data = [
    {
      'icon_url': 'assets/question.png',
      'icon_text': 'I\'m not sure',
    },
    {
      'icon_url': 'assets/info.png',
      'icon_text': 'Information',
    },
    {
      'icon_url': 'assets/customerService.png',
      'icon_text': 'Online help and advice',
    },
    {
      'icon_url': 'assets/shop.png',
      'icon_text': 'Local organisations',
    },
    {
      'icon_url': 'assets/chat.png',
      'icon_text': 'Someone I can text',
    },
    {
      'icon_url': 'assets/chat.png',
      'icon_text': 'Someone I can call',
    },
    {
      'icon_url': 'assets/user.png',
      'icon_text': 'A male I can talk to',
    },
    {
      'icon_url': 'assets/user1.png',
      'icon_text': 'A female I can talk to',
    },
  ];
}

class _ImageGrid2State extends State<ImageGrid2> {
  int _selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, childAspectRatio: 1.5, mainAxisSpacing: 12.h),
        itemCount: widget.icon_data.length,
        itemBuilder: (BuildContext context, int index) {
          // return GridTileBar(
          //   title: InkWell(
          //     onTap: () {
          //       setState(() {
          //         for (int i = 0; i < widget.icon_data.length; i++) {
          //           widget.icon_data[i]['active'] = false;
          //         }
          //         widget.icon_data[index]['active'] = true;
          //       });
          //     },
          //     child: widget.icon_data[index]['active']
          //         ? Stack(children: [
          //             Center(
          //               child: CircleAvatar(
          //                 radius: 50.w,
          //                 backgroundColor: Color.fromRGBO(232, 67, 128, 0.7),
          //                 child: CircleAvatar(
          //                   radius: 46.w,
          //                   backgroundColor: Colors.white,
          //                   child: CircleAvatar(
          //                     radius: 42.w,
          //                     child: Image.asset(
          //                       widget.icon_data[index]['icon_url'],
          //                       fit: BoxFit.contain,
          //                     ),
          //                   ),
          //                 ),
          //               ),
          //             ),
          //             Center(
          //               child: Platform.isAndroid
          //                   ? Padding(
          //                       padding: EdgeInsets.only(top: 10.h),
          //                       child: CircleAvatar(
          //                         backgroundColor:
          //                             Color.fromRGBO(232, 67, 128, 0.85),
          //                         radius: 42.w,
          //                       ),
          //                     )
          //                   : Padding(
          //                       padding: EdgeInsets.only(top: 7.h),
          //                       child: CircleAvatar(
          //                         backgroundColor:
          //                             Color.fromRGBO(232, 67, 128, 0.85),
          //                         radius: 42.w,
          //                       ),
          //                     ),
          //             ),
          //             Center(
          //               child: Padding(
          //                   padding: EdgeInsets.only(top: 35.w),
          //                   child: Image.asset('assets/path973.png',
          //                       height: 30.h, width: 35.w)),
          //             )
          //           ])
          //         : Center(
          //             child: CircleAvatar(
          //               radius: 42.w,
          //               child: Image.asset(
          //                 widget.icon_data[index]['icon_url'],
          //                 fit: BoxFit.contain,
          //               ),
          //             ),
          //           ),
          //   ),
          //   subtitle: Align(
          //     child: Text(
          //       widget.icon_data[index]['icon_text'],
          //       style: TextStyle(color: blackFour, fontSize: 15.sp),
          //       textAlign: TextAlign.center,
          //     ),
          //   ),
          //   // backgroundColor: Colors.red,
          // );
          return GridTileBar(
            title: InkWell(
              onTap: () {
                setState(() {
                  _selectedIndex = index;
                });
              },
              child: _selectedIndex == index
                  ? Stack(children: [
                      Center(
                        child: CircleAvatar(
                          radius: 50.w,
                          backgroundColor: Color.fromRGBO(232, 67, 128, 0.7),
                          child: CircleAvatar(
                            radius: 46.w,
                            backgroundColor: Colors.white,
                            child: CircleAvatar(
                              radius: 42.w,
                              child: Image.asset(
                                widget.icon_data[index]['icon_url'],
                                fit: BoxFit.contain,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Center(
                        child: Platform.isAndroid
                            ? Padding(
                                padding: EdgeInsets.only(top: 8.1.h),
                                child: CircleAvatar(
                                  backgroundColor:
                                      Color.fromRGBO(232, 67, 128, 0.85),
                                  radius: 42.w,
                                ),
                              )
                            : Padding(
                                padding: EdgeInsets.only(top: 7.h),
                                child: CircleAvatar(
                                  backgroundColor:
                                      Color.fromRGBO(232, 67, 128, 0.85),
                                  radius: 42.w,
                                ),
                              ),
                      ),
                      Center(
                        child: Padding(
                            padding: EdgeInsets.only(top: 35.w),
                            child: Image.asset('assets/path973.png',
                                height: 30.h, width: 35.w)),
                      )
                    ])
                  : Center(
                      child: CircleAvatar(
                        radius: 42.w,
                        child: Image.asset(
                          widget.icon_data[index]['icon_url'],
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
            ),
            subtitle: Align(
              child: Text(
                widget.icon_data[index]['icon_text'],
                style: TextStyle(color: blackFour, fontSize: 15.sp),
                textAlign: TextAlign.center,
              ),
            ),
            // backgroundColor: Colors.red,
          );
        });
  }
}
