import 'dart:io' show Platform;
import 'package:flutter/material.dart';
import 'dart:math' as math;
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:youthapp/constant/custom_colors.dart';
import 'package:youthapp/screens/YouthIreland.dart';

class CustomArch3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Padding(
        padding: EdgeInsets.only(top: 15.h),
        child: Stack(children: [
          Positioned(
            child: Platform.isAndroid
                ? GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => YouthIreland()));
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: primary_color,
                      ),
                      height: 70.h,
                      width: 70.w,
                      child: Align(
                        alignment: Alignment.center,
                        child: Container(
                          margin: EdgeInsets.only(left: 5),
                          child: Image.asset(
                            'assets/path738_forward.png',
                            fit: BoxFit.fill,
                            height: 20.h,
                            width: 10.w,
                            alignment: Alignment.center,
                          ),
                        ),
                      ),
                    ),
                  )
                : GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => YouthIreland()));
                    },
                    child: Container(
                      margin: EdgeInsets.only(
                        left: 17.w,
                        top: 5.h,
                      ),
                      // color: Colors.red,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: primary_color,
                      ),
                      height: 70.h,
                      width: 80.w,
                      child: Align(
                        alignment: Alignment.center,
                        child: Container(
                          margin: EdgeInsets.only(left: 5.w),
                          child: Image.asset(
                            'assets/path738_forward.png',
                            fit: BoxFit.fill,
                            height: 20.h,
                            width: 10.w,
                            alignment: Alignment.center,
                          ),
                        ),
                      ),
                    ),
                  ),
          ),
          Container(
            child: CustomPaint(
              // size: Size(10.w, 10.h),
              painter: MyPainter(),
            ),
          ),
        ]),
      ),
    );
  }
}

class MyPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    if (Platform.isAndroid) {
      final rect = Rect.fromCircle(radius: 42.h, center: Offset(35.w, 35.h));
      final startAngle = -math.pi / 2;
      final sweepAngle = math.pi * 2;
      final useCenter = false;
      final paint = Paint()
        ..color = primary_color
        ..style = PaintingStyle.stroke
        ..strokeWidth = 4.r;
      canvas.drawArc(rect, startAngle, sweepAngle, useCenter, paint);
    } else {
      final rect = Rect.fromCircle(radius: 42.h, center: Offset(57.w, 40.h));
      final startAngle = -math.pi / 2;
      final sweepAngle = math.pi * 2;
      final useCenter = false;
      final paint = Paint()
        ..color = primary_color
        ..style = PaintingStyle.stroke
        ..strokeWidth = 4.r;
      canvas.drawArc(rect, startAngle, sweepAngle, useCenter, paint);
    }
  }

  @override
  bool shouldRepaint(CustomPainter old) {
    return false;
  }
}
