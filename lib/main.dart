import 'package:flutter/material.dart';
import 'package:youthapp/screens/intro_screen_1.dart';
import 'package:youthapp/screens/intro_screen_2.dart';
import 'package:youthapp/screens/intro_screen_3.dart';
import 'package:youthapp/widgets/customarch1.dart';

import 'constant/custom_colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      // debugShowMaterialGrid: true,
      // showSemanticsDebugger: true,
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: 'Open Sans',
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ).copyWith(primaryColor: primary_color),
      home: FirstScreen(),
    );
  }
}

class FirstScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        body: ScreenUtilInit(
          designSize: ScreenUtil.defaultSize,
          builder: () => Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 50.r),
            child: IntroScreen1(),
          ),
        ));
  }
}
