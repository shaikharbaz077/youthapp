import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:youthapp/network/customexception.dar/customexception.dart';
import 'package:http/http.dart' as http;

class ApiManager {
  BuildContext context;

  ApiManager(BuildContext context) {
    this.context = context;
  }

  // static Future<bool> checkInternet() async {
  //   var connectivityResult = await (Connectivity().checkConnectivity());
  //   if (connectivityResult == ConnectivityResult.none) {
  //     return false;
  //   } else {
  //     return true;
  //   }
  // }

// Future<dynamic> getFutureCall(
// String url, Map param, BuildContext context) async {
// var responseJson;
// print('Url Was $url');
// print('Parameter $param');
// http.Response response = await http.post(url, body: param);
// try {
// responseJson = _response(response);
// } catch (error) {
// print('Post Call Code $error');
// throw responseJson = _response(response);
// }
// return responseJson;
// }

// Future<dynamic> postCallWidhtContentType(String url, request) async {
// SharedPreferences preferences = await SharedPreferences.getInstance();
// var headers;
// if (preferences.getString(Constant.TOKEN_KEY) == null) {
// headers = {
// "Accept": "application/json",
// };
// print('Null Token $headers');
// } else {
// String userToken = preferences.getString(Constant.TOKEN_KEY);
// print(userToken);
// headers = {"Authorization": "Bearer $userToken"};
// }
// }

// Future<dynamic> postAPICall(String url, Map param) async {
// SharedPreferences prefs = await SharedPreferences.getInstance();
// print('Url $url');
// var headers;
// if (prefs.getString(Constant.TOKEN_KEY) == null) {
// headers = {
// "Accept": "application/json",
// };
// print('Null Token $headers}');
// } else {
// String user = prefs.getString(Constant.TOKEN_KEY);
// print(user);
// headers = {"Authorization": "Bearer $user"};
// print('Have Token $headers');
// }
// print("Calling API: $url");
// print("Calling parameters: $param");
// var responseJson;
// http.Response response =
// await http.post(url, body: param, headers: headers);
// try {
// responseJson = _response(response);
// } catch (error) {
// print('Post Call Code $error');
// throw responseJson = _response(response);
// }
// return responseJson;
// }

  dynamic _response(http.Response response) {
    print('Response Code ${response.statusCode}');
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body);
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
      case 403:
        throw UnauthorisedException(response.body.toString());
      default:
        throw FetchDataException(
            'Error occurred while communication with server with statusCode : ${response.statusCode}');
    }
  }

//   postCallWithHeader(String url, request, BuildContext context) async {
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     var headers;
//     print('Request $request');
//     print('Url $url');
//     if (prefs.getString(Constant.TOKEN_KEY) == null) {
//       headers = {
//         "Accept": "application/json",
//       };
//       print('Null Token $headers}');
//     } else {
//       String user = prefs.getString(Constant.TOKEN_KEY);
//       print(user);
//       headers = {
//         "Accept": "application/json",
//         "Authorization": "Bearer $user",
//       };
//       print('Have Token $headers');
//     }
//     http.Response response;
//     try {
//       response = await http
//           .post(url, body: request, headers: headers)
//           .timeout(Duration(seconds: 30));
//     } catch (e) {
//       print(e);
//     }
//     print('Response ${json.decode(response.body)}');
//     if (response.statusCode == 401) {
//     } else {
//       return await json.decode(response.body);
//     }
//   }

//   postCallWithHeaderContentType(
//       String url, request, BuildContext context) async {
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     var headers;
//     print('Request $request');
//     print('Url $url');
//     if (prefs.getString(Constant.TOKEN_KEY) == null) {
//       headers = {
//         'Content-type': 'application/json',
//       };
//       print('Null Token $headers}');
//     } else {
//       String user = prefs.getString(Constant.TOKEN_KEY);
//       print(user);
//       headers = {"Authorization": "Bearer $user"};
//       print('Have Token $headers');
//     }
//     http.Response response =
//         await http.post(url, body: request, headers: headers);
//     print('Header $headers');
//     print('Response ${json.decode(response.body)}');
//     if (response.statusCode == 401) {
//     } else {
//       return await json.decode(response.body);
//     }
//   }

//   postCallWithHeaderContentTypeBoth(
//       String url, request, BuildContext context) async {
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     var headers;
//     print('Request $request');
//     print('Url $url');
//     if (prefs.getString(Constant.TOKEN_KEY) == null) {
//       headers = {
//         'Content-type': 'application/json',
//       };
//       print('Null Token $headers}');
//     } else {
//       String user = prefs.getString(Constant.TOKEN_KEY);
//       print(user);
//       headers = {
//         'Content-type': 'application/json',
//         "Authorization": "Bearer $user"
//       };
//       print('Have Token $headers');
//     }
//     http.Response response =
//         await http.post(url, body: request, headers: headers);
//     print('Header $headers');
//     print('Response ${json.decode(response.body)}');
//     if (response.statusCode == 401) {
//     } else {
//       return await json.decode(response.body);
//     }
//   }

//   postCallWithParameter(String url, BuildContext context) async {
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     var headers;

//     if (prefs.getString(Constant.TOKEN_KEY) == null) {
//       headers = {"Accept": "application/json"};
//       print('Null Token $headers}');
//     } else {
//       String user = prefs.getString(Constant.TOKEN_KEY);
//       print(user);
//       headers = {"Accept": "application/json", "Authorization": "Bearer $user"};
//       print('Have Token $headers');
//     }

//     http.Response response = await http.post(url, headers: headers);
//     print(url);
//     print(response.body);
//     if (response.statusCode == 401) {
//     } else {
//       return await json.decode(response.body);
//     }
//   }

//   getCall(String url, BuildContext context) async {
//     print(url);
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     var headers;
//     if (prefs.getString(Constant.TOKEN_KEY) == null) {
//       headers = {"Accept": "application/json"};
//     } else {
//       String user = prefs.getString(Constant.TOKEN_KEY);
//       headers = {"Accept": "application/json", "Authorization": 'Bearer $user'};
//     }
//     print(headers);
//     http.Response response = await http.get(url, headers: headers);
//     if (response.statusCode == 401) {
// // AppConstants.logout(context);
// // print("");
//     } else {
//       print('Response Code ${response.statusCode}');
//       print('Response Body ${response.body}');
//       return await jsonDecode(response.body);
//     }
//   }
// }
}
