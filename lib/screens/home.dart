import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:shimmer/shimmer.dart';

import 'package:youthapp/constant/custom_colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:youthapp/constant/custom_colors.dart';
import 'package:youthapp/screens/articals.dart';
import 'package:youthapp/screens/filters.dart';
import 'package:youthapp/screens/home_information.dart';
import 'package:youthapp/widgets/ImageSlideShow.dart';

class Home extends StatelessWidget {
  final text_data = [
    'HSE Psychology Service (Dublin South East)',
    'Balally Family Resource Centre',
    'Jigsaw, the National Centre for Youth Mental Health',
  ];
  final list_data = [
    {
      'image_path': 'assets/maskGroup2_1.png',
      'title': 'How to cope with anxiety about climate change'
    },
    {
      'image_path': 'assets/maskGroup2_2.png',
      'title': 'Domestic violence and what you can do about it!'
    },
    {
      'image_path': 'assets/maskGroup2_3.png',
      'title': 'Resources to support young carers'
    },
    {
      'image_path': 'assets/maskGroup2_4.png',
      'title': 'A step-by-step guide to problem solving'
    },
  ];
  @override
  Widget build(BuildContext context) {
    debugPaintSizeEnabled = false;
    return MaterialApp(
      theme: ThemeData(
        fontFamily: 'Open Sans',
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ).copyWith(primaryColor: primary_color),
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            bottom: TabBar(
              tabs: [
                Tab(
                    child: Text(
                  'Agency',
                  style:
                      TextStyle(fontWeight: FontWeight.w600, fontSize: 16.sp),
                )),
                Tab(
                    child: Text(
                  'Articles',
                  style:
                      TextStyle(fontWeight: FontWeight.w600, fontSize: 16.sp),
                )),
              ],
              indicatorColor: Colors.white,
            ),
            title: Text(
              'Home',
              style: TextStyle(fontWeight: FontWeight.w600, fontSize: 17.sp),
            ),
            centerTitle: true,
            leading: Padding(
              padding: EdgeInsets.only(
                left: 15.w,
                right: 18.w,
                top: 15.h,
                bottom: 12.h,
              ),
              child: Image.asset('assets/more.png'),
            ),
            actions: [
              Padding(
                padding: EdgeInsets.only(right: 8.w),
                child: Image.asset(
                  'assets/group12510.png',
                  width: 21.w,
                  height: 23.h,
                ),
              )
            ],
            backgroundColor: primary_color,
          ),
          body: TabBarView(
            children: [
              Column(
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (_) => Filters()));
                    },
                    child: SizedBox(
                      height: 68.h,
                      child: Container(
                        padding: EdgeInsets.only(
                          left: 15.w,
                          top: 20.h,
                          right: 15.w,
                        ),
                        color: coolBlue10,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Align(
                              alignment: Alignment.center,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Do you need any help or emergency?',
                                    style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: 17.sp,
                                      color: blackFour,
                                    ),
                                  ),
                                  Text(
                                    'Check out some agencies that can help you.',
                                    style: TextStyle(
                                      fontSize: 14.sp,
                                      color: blackFour,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                bottom: 5.h,
                              ),
                              child: Image.asset(
                                'assets/path738.png',
                                height: 13.h,
                                width: 10.w,
                                fit: BoxFit.fill,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemCount: 3,
                      itemBuilder: (_, index) => Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 10.h, horizontal: 15.w),
                        child: Column(
                          children: [
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            HomeInformation(text_data[index])));
                              },
                              child: ImageSlideshow(
                                children: [
                                  FittedBox(
                                    child: Image.asset(
                                      'assets/maskGroup1.png',
                                    ),
                                    fit: BoxFit.fill,
                                  ),
                                  FittedBox(
                                    child: Image.asset(
                                      'assets/maskGroup2.png',
                                    ),
                                    fit: BoxFit.fill,
                                  ),
                                  FittedBox(
                                    fit: BoxFit.fill,
                                    child: Image.asset(
                                      'assets/maskGroup3.png',
                                    ),
                                  ),
                                ],
                                indicatorColor: Colors.white,
                                indicatorBackgroundColor:
                                    Color.fromRGBO(141, 141, 141, 1.0),
                                height: 176.h,
                                width: ScreenUtil().screenWidth,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 10.h),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(
                                    child: Container(
                                      padding: EdgeInsets.only(right: 5.w),
                                      // color: Colors.black38,
                                      child: Text(
                                        text_data[index],
                                        softWrap: true,
                                        style: TextStyle(
                                            fontSize: 17.sp,
                                            fontWeight: FontWeight.w600),
                                      ),
                                    ),
                                  ),
                                  Image.asset(
                                    'assets/callIcon.png',
                                    height: 38.h,
                                    width: 38.w,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  SizedBox(
                    height: 68.h,
                    child: Container(
                      padding: EdgeInsets.only(
                        left: 15.w,
                        top: 20.h,
                        right: 15.w,
                      ),
                      color: coolBlue10,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Align(
                            alignment: Alignment.center,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Are you in an emergency?',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 17.sp,
                                    color: blackFour,
                                  ),
                                ),
                                Text(
                                  'Check out some agencies that can help you.',
                                  style: TextStyle(
                                    fontSize: 14.sp,
                                    color: blackFour,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                              bottom: 5.h,
                            ),
                            child: Image.asset(
                              'assets/path738.png',
                              height: 13.h,
                              width: 10.w,
                              fit: BoxFit.fill,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 20.w,
                      ),
                      child: ListView.builder(
                        itemCount: list_data.length,
                        itemBuilder: (context, index) {
                          return Padding(
                            padding: EdgeInsets.only(top: 20.h),
                            child: HomeScreenTile(
                              imageUrl: list_data[index]['image_path'],
                              text: list_data[index]['title'],
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
          floatingActionButton: Container(
            height: 60.h,
            width: 60.h,
            padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 15.h),
            child: Image.asset(
              'assets/group12207.png',
              // fit: BoxFit.fill,
            ),
            color: Color.fromRGBO(247, 154, 48, 1.0),
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
        ),
      ),
    );
  }
}

class HomeScreenTile extends StatefulWidget {
  final String imageUrl;
  final String text;

  const HomeScreenTile({Key key, this.imageUrl, this.text}) : super(key: key);

  @override
  _HomeScreenTileState createState() => _HomeScreenTileState();
}

class _HomeScreenTileState extends State<HomeScreenTile> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(
            MaterialPageRoute(builder: (_) => Articals(title: widget.text)));
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Image.asset(
            widget.imageUrl,
            height: 80.h,
            width: 80.h,
            fit: BoxFit.fill,
          ),
          SizedBox(
            width: 10.w,
          ),
          Expanded(
            child: Text(
              widget.text,
              style: TextStyle(
                  fontSize: 15.sp,
                  fontWeight: FontWeight.w600,
                  color: greyishBrownSix),
            ),
          ),
        ],
      ),
    );
  }
}
