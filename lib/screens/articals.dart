import 'package:flutter/material.dart';
import 'package:youthapp/constant/custom_colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:youthapp/widgets/aboutcard.dart';
import 'package:youthapp/widgets/agecard.dart';
import 'package:youthapp/widgets/tagscard.dart';

class Articals extends StatelessWidget {
  final title;

  const Articals({Key key, this.title}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Material(
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            leading: Padding(
              padding: EdgeInsets.only(left: 20.w),
              child: Image.asset('assets/path738_backword_white.png'),
            ),
            backgroundColor: coolBlueTwo,
            bottom: TabBar(
              tabs: [
                Tab(
                    child: Text(
                  'Article',
                  style:
                      TextStyle(fontWeight: FontWeight.w600, fontSize: 16.sp),
                )),
                Tab(
                    child: Text(
                  'Info',
                  style:
                      TextStyle(fontWeight: FontWeight.w600, fontSize: 16.sp),
                )),
              ],
              indicatorColor: Colors.white,
            ),
            centerTitle: false,
            leadingWidth: 30.w,
            title: Text(
              title,
              style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.w600),
            ),
          ),
          body: TabBarView(children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  Image.asset(
                    'assets/image19.png',
                    height: 250.h,
                    width: double.infinity,
                    fit: BoxFit.fill,
                  ),
                  Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: 15.w, vertical: 20.h),
                      child: Text(
                        large_text,
                        style: TextStyle(
                            fontSize: 15.sp, color: greyishBrownThree),
                      )),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 15.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  AgeCard(),
                  AboutCard(title: 'This article is for'),
                  Tags(),
                ],
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
