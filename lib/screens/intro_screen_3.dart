import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:youthapp/constant/custom_colors.dart';
import 'package:youthapp/widgets/customarch1.dart';
import 'package:youthapp/widgets/customarch3.dart';

class IntroScreen3 extends StatelessWidget {
  const IntroScreen3({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: primary_color,
      child: Column(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              padding: EdgeInsets.fromLTRB(20.w, 55.h, 20.w, 10.h),
              child: InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'Previous',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15.sp,
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: Stack(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.w),
                  child: Image.asset(
                    'assets/introduction_3.png',
                    // width: 270.w,
                    // height: 792.93,
                    height: 500.h,
                    fit: BoxFit.fill,
                  ),
                ),
                Positioned(
                  bottom: 24.h,
                  left: 5.w,
                  height: 250.h,
                  right: 5.w,
                  // height: 300,
                  child: Container(
                    // color: Colors.white,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(30.r)),
                        color: Colors.white),
                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 16.h),
                          child: Text(
                            'Get urgent help if required',
                            style: TextStyle(
                                fontSize: 19.sp,
                                fontFamily: 'OpenSans',
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 20.w,
                            vertical: 10.h,
                          ),
                          child: Container(
                            child: Text(
                              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 12.sp,
                                  color: Color.fromRGBO(84, 84, 84, 1.0)),
                              softWrap: true,
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        CustomArch3(),
                      ],
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
