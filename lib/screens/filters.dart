import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:youthapp/constant/custom_colors.dart';

class Filters extends StatefulWidget {
  @override
  _FiltersState createState() => _FiltersState();
}

class _FiltersState extends State<Filters> {
  @override
  Widget build(BuildContext context) {
    debugPaintSizeEnabled = false;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Filters',
          style: TextStyle(
            fontWeight: FontWeight.w600,
            fontSize: 17.sp,
          ),
        ),
        actions: [
          Center(
            child: Padding(
              padding: EdgeInsets.only(right: 15.w),
              child: Text(
                'Clear all',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 17.sp,
                ),
              ),
            ),
          )
        ],
      ),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 50.w,
              vertical: 20.h,
            ),
            child: Text(
              'Based on your answer we’ll provide you relevant information.',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 15.sp, color: greyishBrownThree),
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 20.w,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'I am…',
                      style: TextStyle(
                        fontSize: 16.sp,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 9.h),
                      child: Text(
                        '10 - 16yrs',
                        style: TextStyle(
                          fontSize: 15.sp,
                          fontWeight: FontWeight.w600,
                          color: pink,
                        ),
                      ),
                    ),
                  ],
                ),
                Image.asset(
                  'assets/path5095.png',
                  width: 16.4.w,
                  height: 6.1.h,
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 20.h, bottom: 20.h),
            child: Divider(
              color: whiteNine,
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 20.w,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Today I feel…',
                      style: TextStyle(
                        fontSize: 16.sp,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 9.h),
                      child: Text(
                        'Amazing',
                        style: TextStyle(
                          fontSize: 15.sp,
                          fontWeight: FontWeight.w600,
                          color: pink,
                        ),
                      ),
                    ),
                  ],
                ),
                Image.asset(
                  'assets/path5095.png',
                  width: 16.4.w,
                  height: 6.1.h,
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 20.h, bottom: 20.h),
            child: Divider(
              color: whiteNine,
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 20.w,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'I’m looking for information on…',
                        style: TextStyle(
                          fontSize: 16.sp,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 9.h),
                        child: Text(
                          'Anxiety, Addiction, Discrimination, Drugs, Depression, Family Conflict',
                          softWrap: true,
                          style: TextStyle(
                            fontSize: 15.sp,
                            fontWeight: FontWeight.w600,
                            color: pink,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.h),
                  child: Image.asset(
                    'assets/path5095.png',
                    width: 16.4.w,
                    height: 6.1.h,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 20.h, bottom: 20.h),
            child: Divider(
              color: whiteNine,
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 20.w,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'I’m looking for…',
                        style: TextStyle(
                          fontSize: 16.sp,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 9.h),
                        child: Text(
                          'I’m not sure, Someone I can call, Information',
                          softWrap: true,
                          style: TextStyle(
                            fontSize: 15.sp,
                            fontWeight: FontWeight.w600,
                            color: pink,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.h),
                  child: Image.asset(
                    'assets/path5095.png',
                    width: 16.4.w,
                    height: 6.1.h,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: SizedBox(
                width: double.infinity,
                height: 80.h,
                child: RaisedButton(
                  onPressed: () {
                    // Navigator.of(context).push(
                    //     MaterialPageRoute(builder: (_) => TodayFeel()));
                  },
                  color: coolBlueTwo,
                  child: Text(
                    'Apply filters',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontSize: 17.sp),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
