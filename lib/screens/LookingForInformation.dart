import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:youthapp/constant/custom_colors.dart';
import 'package:youthapp/screens/LookingFor.dart';
import 'package:youthapp/widgets/FilterChip.dart';

class LookingForInformation extends StatelessWidget {
  List<Map<String, Object>> _chip_data = [
    {
      'label_name': 'Abuse',
      'active': false,
    },
    {
      'label_name': 'Addiction',
      'active': false,
    },
    {
      'label_name': 'Alcohol',
      'active': false,
    },
    {
      'label_name': 'Anger',
      'active': false,
    },
    {
      'label_name': 'Anxiety',
      'active': false,
    },
    {
      'label_name': 'Body Image',
      'active': false,
    },
    {
      'label_name': 'Bullying',
      'active': false,
    },
    {
      'label_name': 'Contraception',
      'active': false,
    },
    {
      'label_name': 'Dating',
      'active': false,
    },
    {
      'label_name': 'Depression',
      'active': false,
    },
    {
      'label_name': 'Discrimination',
      'active': false,
    },
    {
      'label_name': 'Eating disorders',
      'active': false,
    },
    {
      'label_name': 'Grief',
      'active': false,
    },
    {
      'label_name': 'Family Conflict',
      'active': false,
    },
    {
      'label_name': 'Drugs',
      'active': false,
    },
    {
      'label_name': 'Gambling',
      'active': false,
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Padding(
          padding: EdgeInsets.only(top: 45.h),
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 15.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Image.asset(
                        'assets/path738_backword.png',
                        fit: BoxFit.fill,
                        height: 15.h,
                        // width: ,
                      ),
                    ),
                    Text(
                      'Step 3/4',
                      style: TextStyle(color: Color.fromRGBO(84, 84, 84, 1.0)),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: 20.h,
                ),
                child: Text(
                  'I’m looking for information on...',
                  style: TextStyle(
                    fontSize: 22.sp,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 20.w, vertical: 20.h),
                  child: Wrap(
                    alignment: WrapAlignment.center,
                    spacing: 10.w,
                    runSpacing: 10.h,
                    children:
                        _chip_data.map((e) => CustomFilterChip(e)).toList(),
                  ),
                ),
              ),
              SizedBox(
                width: double.infinity,
                height: 80.h,
                child: RaisedButton(
                  onPressed: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (_) => LookingFor()));
                  },
                  color: primary_color,
                  child: Text(
                    'Next',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontSize: 17.sp),
                  ),
                ),
              )
              // CustomFilterChip(),
            ],
          ),
        ));
  }
}
