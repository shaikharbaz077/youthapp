import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:youthapp/constant/custom_colors.dart';
import 'package:youthapp/widgets/Imageslideshow2.dart';
import 'package:youthapp/widgets/aboutcard.dart';
import 'package:youthapp/widgets/address.dart';
import 'package:youthapp/widgets/agecard.dart';
import 'package:youthapp/widgets/contact.dart';
import 'package:youthapp/widgets/imageLoad.dart';
import 'package:youthapp/widgets/tagscard.dart';

class HomeInformation extends StatelessWidget {
  final title;

  HomeInformation(this.title);
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: SingleChildScrollView(
        child: Stack(
          children: [
            ImageSlideshow2(
              children: [
                FittedBox(
                  child: Image.asset(
                    'assets/maskGroup1.png',
                  ),
                  fit: BoxFit.fitHeight,
                ),
                FittedBox(
                  child: Image.asset(
                    'assets/maskGroup2.png',
                  ),
                  fit: BoxFit.fitHeight,
                ),
                FittedBox(
                  child: Image(
                    image: AssetImage('assets/maskGroup3.png'),
                  ),
                  fit: BoxFit.fill, // use th
                )
              ],
              indicatorColor: Colors.white,
              indicatorBackgroundColor: Color.fromRGBO(141, 141, 141, 1.0),
              height: 250.h,
              width: ScreenUtil().screenWidth,
            ),
            ImageLoad(),
            Container(
              width: double.infinity,
              // color: Colors.white,
              margin: EdgeInsets.only(top: 235.h),
              padding: EdgeInsets.symmetric(
                horizontal: 15.w,
                vertical: 10.h,
              ),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10.r),
                    topLeft: Radius.circular(10.r),
                  )),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: TextStyle(
                      fontSize: 20.sp,
                      fontWeight: FontWeight.w600,
                      color: blackFour,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10.h),
                    child: Text(
                      semi_large_text,
                      style:
                          TextStyle(fontSize: 14.sp, color: greyishBrownThree),
                    ),
                  ),
                  Contact(),
                  Address(),
                  AgeCard(),
                  AboutCard(title: 'This agency supports'),
                  Tags(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
