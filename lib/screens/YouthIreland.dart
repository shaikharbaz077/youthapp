import 'package:flutter/material.dart';
import 'package:youthapp/constant/custom_colors.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:youthapp/screens/TodayFeel.dart';
import 'package:youthapp/widgets/AgeSelectionButton.dart';

class YouthIreland extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.only(top: 45.h),
        child: Column(
          children: [
            Align(
              alignment: Alignment.centerRight,
              child: Padding(
                child: Text(
                  'Step 1/4',
                  style: TextStyle(color: Color.fromRGBO(84, 84, 84, 1.0)),
                ),
                padding: EdgeInsets.only(right: 18.w),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                  left: 30.w, top: 20.h, right: 30.w, bottom: 10.h),
              child: Text(
                'Welcome to the Youth work Ireland',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: primary_color,
                    fontWeight: FontWeight.w700,
                    fontSize: 30.sp),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 50.w),
              child: Text(
                'Based on your answer we’ll provide you with relevant information.',
                textAlign: TextAlign.center,
                style: TextStyle(color: blackFour, fontSize: 15.sp),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20.h),
              child: Text(
                'I am…',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 22.sp),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 20.h),
                    child: AgeSelectionButton(),
                  ),
                  SizedBox(
                    width: double.infinity,
                    height: 80.h,
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).push(
                            MaterialPageRoute(builder: (_) => TodayFeel()));
                      },
                      color: primary_color,
                      child: Text(
                        'Next',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 17.sp),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
