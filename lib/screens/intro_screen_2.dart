import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:youthapp/constant/custom_colors.dart';

import 'package:youthapp/widgets/customarch2.dart';

class IntroScreen2 extends StatelessWidget {
  const IntroScreen2({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: primary_color,
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(20.w, 55.h, 20.w, 10.h),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Container(
                    child: Text(
                      'Previous',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 15.sp,
                      ),
                    ),
                  ),
                ),
                InkWell(
                  onTap: () {},
                  child: Container(
                    child: Text(
                      'Skip',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 15.sp,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Stack(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.w),
                  child: Image.asset(
                    'assets/introduction_2.png',
                    // width: 270.w,
                    // height: 792.93,
                    height: 500.h,
                    fit: BoxFit.fill,
                  ),
                ),
                Positioned(
                  bottom: 24.h,
                  left: 5.w,
                  height: 250.h,
                  right: 5.w,
                  // height: 300,
                  child: Container(
                    // color: Colors.white,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(30.r)),
                        color: Colors.white),
                    child: Column(
                      // mainAxisSize: MainAxisSize.min,

                      children: [
                        Padding(
                          // padding: EdgeInsets.symmetric(
                          //     horizontal: 30.w, vertical: 10.h),
                          padding: EdgeInsets.only(
                              top: 25.h, left: 20.w, right: 20.w),
                          child: Text(
                            'Get relevant contact details and articles',
                            style: TextStyle(
                                fontSize: 18.sp,
                                fontFamily: 'OpenSans',
                                fontWeight: FontWeight.w600),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 20.w,
                            vertical: 5.h,
                          ),
                          child: Container(
                            child: Text(
                              'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 12.sp,
                                  color: Color.fromRGBO(84, 84, 84, 1.0)),
                              softWrap: true,
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        Expanded(child: CustomArch2()),
                      ],
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
