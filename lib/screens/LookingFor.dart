import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:youthapp/constant/custom_colors.dart';
import 'package:youthapp/screens/home.dart';
import 'package:youthapp/widgets/ImageGrid.dart';
import 'package:youthapp/widgets/ImageGrid2.dart';

class LookingFor extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: EdgeInsets.only(top: 45.h),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Image.asset(
                      'assets/path738_backword.png',
                      fit: BoxFit.fill,
                      height: 15.h,
                      // width: ,
                    ),
                  ),
                  Text(
                    'Step 4/4',
                    style: TextStyle(color: Color.fromRGBO(84, 84, 84, 1.0)),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20.h),
              child: Text(
                'I’m looking for…',
                style: TextStyle(
                  fontSize: 22.sp,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
            Expanded(
              child: Container(
                child: ImageGrid2(),
                // height: 508.h,
              ),
            ),
            SizedBox(
              width: double.infinity,
              height: 80.h,
              child: RaisedButton(
                onPressed: () {
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(builder: (_) => Home()),
                      (Route<dynamic> route) => false);
                },
                color: primary_color,
                child: Text(
                  'Submit',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 17.sp),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
